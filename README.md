# **UMT_IT2PythonProject_FinalVersion**

## **Introduction**

This is a Python project for Information Technology Fundamentals 2 module's final examination at Ho Chi Minh City University of Management and Technology.

Our team has three members:

* Phan Vinh Tien.

* Phan Nguyen Duy Kha.

* Hoang Quang Huy.

## **Instruction**

To run this project, please follow these below steps:

* **Step 1.** Open **Terminal** in the directory that you want to store this project.

* **Step 2.** Respectively run one-by-one these below commands.

```
git clone git@gitlab.com:vinhtienlovemath/UMT_IT2PythonProject_FinalVersion.git

cd ./UMT_IT2PythonProject_FinalVersion/GAME2D_IT2

code .
```

* **Step 3.** Then the Visual Studio Code is opened. You just have to run the `main.py` file in the `./GAME2D_IT2/2_Level/code/`. A pygame window will be shown.

<p align="center">
<img src="./Introduction.png">
</p>
