import pygame
from support import import_csv_layout, import_cut_graphics
from setting import tile_size, screen_height, screen_width
from tiles import Tile, StaticTile, AnimatedTile, Coin, Tree
from enemy import Enemy
from decoration import Sky, Water, Clouds
from player import Player
from particles import ParticleEffect
from game_data import levels
from music import Music

pygame.mixer.init()
effect_music = Music()

class Level:
    def __init__(self, current_level, surface, create_overworld, change_coins, change_health):

        # Setup tổng quan
        self.display_surface = surface          
        self.world_shift = 0
        self.current_x = None

        # Audio
        self.coin_sound = effect_music.coin_sound
        self.stomp_sound = effect_music.stomp_sound

        # Kết nối overworld
        self.create_overworld = create_overworld
        self.current_level = current_level
        level_data = levels[self.current_level]
        self.new_max_level = level_data['unlock']

        # Player
        player_layout = import_csv_layout(level_data['player'])     #Setup con player 
        self.player = pygame.sprite.GroupSingle()   
        self.goal = pygame.sprite.GroupSingle() 
        self.player_setup(player_layout, change_health)            

        # Khởi tạo biến thay đổi coin
        self.change_coins = change_coins

        # Dust
        self.dust_sprite = pygame.sprite.GroupSingle()
        self.player_on_ground = False

        # Tạo chạm con quái
        self.explosion_sprites = pygame.sprite.Group()

        # Setup địa hình
        terrain_layout = import_csv_layout(level_data['terrain'])
        self.terrain_sprites = self.create_tile_group(terrain_layout,'terrain')

        # Setup grass
        grass_layout = import_csv_layout(level_data['grass'])
        self.grass_sprites = self.create_tile_group(grass_layout,'grass')

        # Setup coin
        coin_layout  = import_csv_layout(level_data['coin'])
        self.coin_sprites = self.create_tile_group(coin_layout,'coin')

        # Setup tree
        tree_layout  = import_csv_layout(level_data['tree'])
        self.tree_sprites = self.create_tile_group(tree_layout,'tree')  

        # Enemy (con quái)
        enemy_layout  = import_csv_layout(level_data['enemies'])
        self.enemy_sprites = self.create_tile_group(enemy_layout,'enemies') 

        # Constraint (Giới hạn con quái di chuyển)
        constraint_layout  = import_csv_layout(level_data['constraint'])
        self.constraint_sprites = self.create_tile_group(constraint_layout,'constraint')   

        # Decoration
        self.Sky = Sky(6)
        level_width = len(terrain_layout[0]) * tile_size
        self.water = Water(screen_height - 40,level_width )
        self.cloud = Clouds(150,level_width,20) 

    # Tạo địa hình từ file csv
    def create_tile_group(self,layout,type):   # Tạo tile
        sprite_group = pygame.sprite.Group()
        
        # Tạo địa hình
        for row_index, row in enumerate(layout):
            for col_index,val in enumerate(row):
                if val != '-1':  # Nếu địa chỉ khác -1 thì cho hiển thị tile
                    x = col_index * tile_size # 
                    y = row_index * tile_size #

                    if type == 'terrain':  
                        terrian_tile_list = import_cut_graphics('../GAME2D_IT2/2_Level/graphics/terrain/terrain_tiles.png')
                        tile_surface = terrian_tile_list[int(val)]
                        sprite = StaticTile(tile_size,x,y,tile_surface) 

                    if type == 'grass':  
                        grass_tile_list = import_cut_graphics('../GAME2D_IT2/2_Level/graphics/decoration/grass/grass.png')
                        tile_surface = grass_tile_list[int(val)]
                        sprite = StaticTile(tile_size,x,y,tile_surface) 

                    if type == 'coin':  
                        if val == '0': sprite = Coin(tile_size,x,y,'../GAME2D_IT2/2_Level/graphics/coins/gold',5)
                        if val == '1': sprite = Coin(tile_size,x,y,'../GAME2D_IT2/2_Level/graphics/coins/silver',3) 

                    if type == 'tree':  
                        tree_tile_list = import_cut_graphics('../GAME2D_IT2/2_Level/graphics/terrain/tree_1/tree_1.png')
                        tile_surface = tree_tile_list[int(val)]
                        sprite = StaticTile(tile_size,x,y,tile_surface) 
                    
                    if type == 'enemies':
                        sprite = Enemy(tile_size,x,y)

                    if type == 'constraint':
                        sprite = Tile(tile_size,x,y)

                    sprite_group.add(sprite)

        return sprite_group 
    
    # Tạo Player
    def player_setup(self,layout, change_health):
        for row_index, row in enumerate(layout):
            for col_index,val in enumerate(row):
                x = col_index * tile_size # 
                y = row_index * tile_size #
                if val == '0':  # Nếu địa chỉ khác -1 thì cho hiển thị tile
                    sprite = Player((x,y),self.display_surface,self.create_jump_particles, change_health) 
                    self.player.add(sprite)
                if val == '1':
                    hat_surface = pygame.image.load('../GAME2D_IT2/2_Level/graphics/character/star.png').convert_alpha()
                    sprite = StaticTile(tile_size,x,y,hat_surface)
                    self.goal.add(sprite)

    # Tạo con quái
    def enemy_collision_reverse(self):
        for enemy in self.enemy_sprites.sprites():
            if pygame.sprite.spritecollide(enemy,self.constraint_sprites,False):
                enemy.reverse()


    def create_jump_particles(self,pos):
        if self.player.sprite.facing_right:
            pos -= pygame.math.Vector2(10,5)
        else:
            pos += pygame.math.Vector2(10,5)
        jump_particle_sprite = ParticleEffect(pos,'jump')
        self.dust_sprite.add(jump_particle_sprite)

      
    # Tạo va chạm ngang
    def horizontal_movement_collision(self):
        player = self.player.sprite
        player.rect.x += player.direction.x * player.speed
        collidable_sprites = self .terrain_sprites.sprites()

        for sprite in collidable_sprites:
            if sprite.rect.colliderect(player.rect):
                if player.direction.x < 0:
                    player.rect.left = sprite.rect.right
                    player.on_left = True
                    self.current_x = player.rect.left
                elif player.direction.x > 0:
                    player.rect.right = sprite.rect.left
                    player.on_right = True
                    self.current_x = player.rect.right

        if player.on_left and (player.rect.left < self.current_x or player.direction.x >= 0):
            player.on_left = False
        if player.on_right and (player.rect.right > self.current_x or player.direction.x <= 0):
            player.on_right = False

    # Tạo va chạm dọc   
    def vertical_movement_collision(self):      
        player = self.player.sprite
        player.apply_gravity()
        collidable_sprites = self.terrain_sprites.sprites()

        for sprite in collidable_sprites:
            if sprite.rect.colliderect(player.rect):
                if player.direction.y > 0:
                    player.rect.bottom = sprite.rect.top
                    player.direction.y = 0 
                    player.on_ground = True
                elif player.direction.y < 0:
                    player.rect.top = sprite.rect.bottom 
                    player.direction.y = 0
                    player.on_ceiling = True

        if player.on_ground and player.direction.y < 0 or player.direction.y > 1:
            player.on_ground = False
        if player.on_ceiling and player.direction.y > 0:
            player.on_ceiling = False  
   
    # Tạo cơ chế di chuyển con nhân vật và màn hình
    def scroll_x(self):
        player = self.player.sprite
        player_x = player.rect.centerx
        direction_x = player.direction.x

        if player_x < screen_width /3 and direction_x < 0:
            self.world_shift = 8
            player.speed = 0
        elif player_x > screen_width - (screen_width / 3) and direction_x > 0:
                self.world_shift = -8
                player.speed = 0
        else:
                self.world_shift = 0
                player.speed = 8

    # Con nv chết queo
    def check_death(self):
        if self.player.sprite.rect.top > screen_height:
            self.coins = 0
            self.create_overworld(self.current_level,0)
            
            

    def check_win(self):
        if pygame.sprite.spritecollide(self.player.sprite,self.goal,False):
            
            self.create_overworld(self.current_level,0)
            self.create_overworld(self.current_level,self.new_max_level)
    
            
    def check_coin_collisions(self):
        collided_coins = pygame.sprite.spritecollide(self.player.sprite,self.coin_sprites,True)
        if collided_coins:
            self.coin_sound.play()
            for coin in collided_coins:
                self.change_coins(coin.value)

    def check_enemy_collisions(self):
        enemy_collisions = pygame.sprite.spritecollide(self.player.sprite,self.enemy_sprites,False)
        if enemy_collisions:
            for enemy in enemy_collisions:
                enemy_center = enemy.rect.centery
                enemy_top = enemy.rect.top
                player_bottom = self.player.sprite.rect.bottom

                if enemy_top < player_bottom < enemy_center and self.player.sprite.direction.y >= 0:
                    self.stomp_sound.play()
                    self.player.sprite.direction.y = -15
                    explosion_sprite = ParticleEffect(enemy.rect.center,'explosion')
                    self.explosion_sprites.add(explosion_sprite)
                    enemy.kill()
                    self.change_coins(10)
                else:
                    self.player.sprite.get_damage()

    def run(self):
        # decoratiom
        self.Sky.draw(self.display_surface)
        self.cloud.draw(self.display_surface, self.world_shift)

        # Terrian
        self.terrain_sprites.update(self.world_shift)     # Chạy lia màng hình
        self.terrain_sprites.draw(self.display_surface)   # Hiểu thị tile địa hình

        # Grass
        self.grass_sprites.update(self.world_shift)       # Chạy lia màng hình
        self.grass_sprites.draw(self.display_surface)     # Hiển thị tile grass 
        
        # Tree
        self.tree_sprites.update(self.world_shift)          # Chạy lia màng hình
        self.tree_sprites.draw(self.display_surface)        # Hiển thị tile tree
       
        # Enemy
        self.enemy_sprites.update(self.world_shift)          # Chạy lia màng hình
        self.constraint_sprites.update(self.world_shift)
        self.enemy_collision_reverse()
        self.enemy_sprites.draw(self.display_surface)
                # Hiển thị tile enemy
        self.explosion_sprites.update(self.world_shift)
        self.explosion_sprites.draw(self.display_surface)

        # Coin
        self.coin_sprites.update(self.world_shift)          # Chạy lia màng hình
        self.coin_sprites.draw(self.display_surface)        # Hiển thị tile coin

        # Player 
        self.player.update()
        self.horizontal_movement_collision()
        self.vertical_movement_collision()
        self.scroll_x()
        self.player.draw(self.display_surface)
        self.goal.update(self.world_shift)
        self.goal.draw(self.display_surface)

        self.check_death()
        self.check_win()

        self.check_coin_collisions()
        self.check_enemy_collisions()

        # Water
        self.water.draw(self.display_surface,self.world_shift)
