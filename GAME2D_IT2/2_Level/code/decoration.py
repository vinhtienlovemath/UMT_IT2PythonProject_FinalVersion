from setting import vertical_tile_number,tile_size,screen_width
from tiles import AnimatedTile, StaticTile
from support import import_folder
from random import choice, randint
import pygame


class Sky:
    def __init__(self, horizon):
        self.Skyy = pygame.image.load('../GAME2D_IT2/2_Level/graphics/decoration/sky/Skyy.png').convert()
        self.Skyy_middle = pygame.image.load('../GAME2D_IT2/2_Level/graphics/decoration/sky/Skyy_middle.png').convert()
        self.Skyy_bottom = pygame.image.load('../GAME2D_IT2/2_Level/graphics/decoration/sky/Skyy_bottom.png').convert()
        self.horizon = horizon

        self.Skyy = pygame.transform.scale(self.Skyy, (screen_width, tile_size))
        self.Skyy_middle = pygame.transform.scale(self.Skyy_middle, (screen_width, tile_size))
        self.Skyy_bottom = pygame.transform.scale(self.Skyy_bottom, (screen_width, tile_size))

    def draw(self,surface):   # Cho background chạy lên màn hình
        for row in range(vertical_tile_number):
            y = row * tile_size
            if row < 2* self.horizon:
                surface.blit(self.Skyy,(0,y))
            elif row == self.horizon:
                surface.blit(self.Skyy_middle,(0,y)) 

class Water:    # Class Water
    def __init__(self,top,level_width):   
        water_start =  -screen_width
        water_tile_width = 192
        tile_x_amount = int((level_width + screen_width + 1000) / water_tile_width)
        self.water_sprites = pygame.sprite.Group()

        for tile in range(tile_x_amount):
            x = tile * water_tile_width + water_start
            y = top
            sprite = AnimatedTile(192,x,y,'../GAME2D_IT2/2_Level/graphics/decoration/water')
            self.water_sprites.add(sprite)

    def draw(self,surface,shift):
        self.water_sprites.update(shift)
        self.water_sprites.draw(surface)

class Clouds:
    def __init__(self,horizon,level_width,cloud_number):
        cloud_surf_list = import_folder('../GAME2D_IT2/2_Level/graphics/decoration/clouds')
        min_x = - screen_width - 200
        max_x = level_width + screen_width
        min_y = 0
        max_y = horizon / 2
        self.cloud_sprites = pygame.sprite.Group() 

        for cloud in range(cloud_number):
            cloud = choice(cloud_surf_list)
            x = randint(min_x,max_x)
            y = randint(min_y,max_y)
            sprite = StaticTile(0,x,y,cloud)
            self.cloud_sprites.add(sprite)


    def draw(self,surface,shift):
        self.cloud_sprites.update(shift)
        self.cloud_sprites.draw(surface)