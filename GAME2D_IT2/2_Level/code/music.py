import pygame

class Music:
    def __init__(self):
        # Background music
        self.menu_bg_music = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/overworld_music.wav')
        self.level_bg_music = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/level_music.wav')
        self.overworld_bg_music = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/overworld_music.wav')
        
        # Effect sounds
        self.coin_sound = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/effects/coin.wav')
        self.stomp_sound = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/effects/stomp.wav')
        self.jump_sound = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/effects/jump.wav')
        self.hit_sound = pygame.mixer.Sound('../GAME2D_IT2/2_Level/audio/effects/hit.wav')

        