import pygame, sys
from setting import *
from level import Level
from overworld import Overworld
from hud import HUD
from button import Button
from music import Music

pygame.mixer.init()
bg_music = Music()
menu_bg_music = bg_music.menu_bg_music


class Game:
	def __init__(self):
		# Audio
		self.level_bg_music = bg_music.level_bg_music
		self.overworld_bg_music = bg_music.overworld_bg_music
		self.overworld_bg_music.play(loops=-1)
                
		self.max_level = 0
		self.max_health = 100
		self.cur_health = 100
		self.coins = 0

		self.overworld = Overworld(0, self.max_level, screen, self.create_level)
		self.status = 'overworld'

		self.hud = HUD(screen)

	def create_level(self, current_level):
		self.overworld_bg_music.stop()
		self.level_bg_music.play(loops=-1)
		
		self.level = Level(current_level, screen, self.create_overworld, self.change_coin, self.change_health)
		self.status = 'level'

	def create_overworld(self, current_level, new_max_level):
		self.level_bg_music.stop()
		self.overworld_bg_music.play(loops=-1)
		
		if new_max_level > self.max_level:
			self.max_level = new_max_level
		self.overworld = Overworld(current_level, self.max_level, screen, self.create_level)
		self.status = 'overworld'

	# Tạo sự thay đổi coin
	def change_coin(self, amount):
		self.coins += amount	

	# Tạo sự thay đổi máu
	def change_health(self, amount):
		self.cur_health += amount

	# Tạo sự chết khi hết máu
    # Nếu máu <= 0 thì sẽ đưa về màn hình overworld với level hiện tại và reset coin = 0
	def check_game_over(self):
		if self.cur_health <= 0:
			self.coins = 0
			self.create_overworld(self.level.current_level, self.max_level)  
			self.level_bg_music.stop()
			self.overworld_bg_music.play(loops=-1)

	def run(self):          
		if self.status == 'overworld':
			self.overworld.run()
			self.cur_health = 100
		else:
			self.level.run()
			self.hud.show_health(self.cur_health, self.max_health)
			self.hud.show_coins(self.coins)
			self.check_game_over()

""" Các function cho menu """
menu_background = pygame.image.load('../GAME2D_IT2/2_Level/graphics/decoration/sky/background.png')
menu_background = pygame.transform.scale(menu_background, (screen_width, screen_height))

def get_font(size): 
    return pygame.font.Font('../GAME2D_IT2/2_Level/graphics/buttons/menu_font_1.ttf', size)

def play():
	menu_bg_music.stop()
	clock = pygame.time.Clock()
	game = Game()
					
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			if event.type == pygame.MOUSEBUTTONDOWN:
				if PLAY_BACK.check_for_input(PLAY_MOUSE_POS):
					game.overworld_bg_music.stop()               
					menu_bg_music.play(loops=-1)
					main_menu()
		screen.fill('grey')
		game.run()
                
		if game.status == 'overworld':
			PLAY_MOUSE_POS = pygame.mouse.get_pos()		
			PLAY_BACK = Button(image=None, pos=(1080, 40), 
								text_input="MENU", font=get_font(50), base_color="White", hovering_color="Green")
			PLAY_BACK.change_color(PLAY_MOUSE_POS)
			PLAY_BACK.update(screen)
		pygame.display.update()
		clock.tick(60)
    
def instructions():
    menu_bg_music.stop()
    while True:
        INSTRUCTIONS_MOUSE_POS = pygame.mouse.get_pos()

        screen.fill("white")

        INSTRUCTIONS_TEXT = get_font(75).render("INSTRUCTION", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 100))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)
        
        INSTRUCTIONS_TEXT = get_font(25).render("In Overworld, press Enter to join to the level.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 175))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)
        
        INSTRUCTIONS_TEXT = get_font(25).render("Use Left/Right key to choose the target level.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 225))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)
        
        INSTRUCTIONS_TEXT = get_font(25).render("Use Left/Right key to move, and SPACE to jump.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 275))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)
        
        INSTRUCTIONS_TEXT = get_font(25).render("Each time the character collides with the enemy, the health bar is decreased by 25%.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 325))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)
        
        INSTRUCTIONS_TEXT = get_font(25).render("The character also can attack the enemies from the head to obtain points.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 375))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)

        INSTRUCTIONS_TEXT = get_font(25).render("When the character touches the star at the end, new level will be unlocked.", True, "Black")
        INSTRUCTIONS_RECT = INSTRUCTIONS_TEXT.get_rect(center=(615, 425))
        screen.blit(INSTRUCTIONS_TEXT, INSTRUCTIONS_RECT)

        INSTRUCTIONS_BACK = Button(image=None, pos=(615, 500), 
                            text_input="BACK", font=get_font(40), base_color="Black", hovering_color="Green")

        INSTRUCTIONS_BACK.change_color(INSTRUCTIONS_MOUSE_POS)
        INSTRUCTIONS_BACK.update(screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if INSTRUCTIONS_BACK.check_for_input(INSTRUCTIONS_MOUSE_POS):
                    main_menu()
        pygame.display.update()

def about():
    menu_bg_music.stop()
    while True:
        ABOUT_MOUSE_POS = pygame.mouse.get_pos()

        screen.fill("white")

        ABOUT_TEXT = get_font(75).render("HKT GAME", True, "Black")
        ABOUT_RECT = ABOUT_TEXT.get_rect(center=(615, 100))
        screen.blit(ABOUT_TEXT, ABOUT_RECT)
        
        ABOUT_TEXT = get_font(25).render("Developer, SFX/Music, UI: Vinh Tien", True, "Black")
        ABOUT_RECT = ABOUT_TEXT.get_rect(center=(615, 175))
        screen.blit(ABOUT_TEXT, ABOUT_RECT)
        
        ABOUT_TEXT = get_font(25).render("Producer, Developer, Designer, Artist: Duy Kha", True, "Black")
        ABOUT_RECT = ABOUT_TEXT.get_rect(center=(615, 250))
        screen.blit(ABOUT_TEXT, ABOUT_RECT)
        
        ABOUT_TEXT = get_font(25).render("Developer: Quang Huy", True, "Black")
        ABOUT_RECT = ABOUT_TEXT.get_rect(center=(615, 325))
        screen.blit(ABOUT_TEXT, ABOUT_RECT)

        ABOUT_BACK = Button(image=None, pos=(615, 500), 
                            text_input="BACK", font=get_font(40), base_color="Black", hovering_color="Green")

        ABOUT_BACK.change_color(ABOUT_MOUSE_POS)
        ABOUT_BACK.update(screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if ABOUT_BACK.check_for_input(ABOUT_MOUSE_POS):
                    main_menu()

        pygame.display.update()

def main_menu():
    menu_bg_music.play(loops=-1)
    while True:
        screen.blit(menu_background, (0, 0))

        MENU_MOUSE_POS = pygame.mouse.get_pos()

        MENU_TEXT = get_font(100).render("MAIN MENU", True, "#b68f40")
        MENU_RECT = MENU_TEXT.get_rect(center=(640, 75))

        PLAY_BUTTON = Button(image=pygame.image.load('../GAME2D_IT2/2_Level/graphics/buttons/play_rect.png'), pos=(640, 220), text_input="PLAY", font=get_font(75), base_color="#d7fcd4", hovering_color="White")
        
        ABOUT_BUTTON = Button(image=pygame.image.load('../GAME2D_IT2/2_Level/graphics/buttons/about_rect.png'), pos=(640, 490), text_input="ABOUT", font=get_font(75), base_color="#d7fcd4", hovering_color="White")
        
        INSTRUCTIONS_BUTTON = Button(image=pygame.image.load('../GAME2D_IT2/2_Level/graphics/buttons/about_rect.png'), pos=(640, 355), text_input="HELP", font=get_font(75), base_color="#d7fcd4", hovering_color="White")
        
        QUIT_BUTTON = Button(image=pygame.image.load('../GAME2D_IT2/2_Level/graphics/buttons/quit_rect.png'), pos=(640, 625), text_input="QUIT", font=get_font(75), base_color="#d7fcd4", hovering_color="White")

        screen.blit(MENU_TEXT, MENU_RECT)

        for button in [PLAY_BUTTON, ABOUT_BUTTON, INSTRUCTIONS_BUTTON, QUIT_BUTTON]:
            button.change_color(MENU_MOUSE_POS)
            button.update(screen)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if PLAY_BUTTON.check_for_input(MENU_MOUSE_POS):
                    play()
                
                if ABOUT_BUTTON.check_for_input(MENU_MOUSE_POS):
                    about()

                if INSTRUCTIONS_BUTTON.check_for_input(MENU_MOUSE_POS):
                    instructions()

                if QUIT_BUTTON.check_for_input(MENU_MOUSE_POS):
                    pygame.quit()
                    sys.exit()

        pygame.display.update()

if __name__ == "__main__":
	# Pygame setup
	pygame.init()
	music_pause = False
	screen = pygame.display.set_mode((screen_width,screen_height))  # Tạo một màn hình pygame
	pygame.display.set_caption('HKT Game')
	icon = pygame.image.load('../GAME2D_IT2/2_Level/graphics/window/character.png')
	pygame.display.set_icon(icon)
	main_menu()



