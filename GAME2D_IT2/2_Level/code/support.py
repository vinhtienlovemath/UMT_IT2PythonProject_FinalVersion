from csv import reader
from setting import tile_size
from os import walk
import pygame

def import_folder(path):
    surface_list = []

    for _,__,image_files in walk(path):
        for image in image_files:
            full_path = path + '/' + image
            image_surf = pygame.image.load(full_path).convert_alpha()
            surface_list.append(image_surf)

    return surface_list

def import_csv_layout(path):
    terrain_map= []

     # Tạo thư viện mở dẫn tới mục
    with open(path) as map:
        level = reader(map,delimiter = ',')
        for row in level:
            terrain_map.append(list(row))
        return terrain_map
    
def import_cut_graphics(path):
    suface = pygame.image.load(path).convert_alpha()
    tile_num_x = int(suface.get_size()[0] / tile_size)
    tile_num_y = int(suface.get_size()[0] / tile_size)

    cut_tiles = []
    for row in range(tile_num_x):
        for col in range(tile_num_y):
            x = col * tile_size
            y = row * tile_size
            new_surf = pygame.Surface((tile_size,tile_size), flags = pygame.SRCALPHA )
            #new_surf = pygame.Surface((tile_size,tile_size))
            new_surf.blit(suface,(0,0),pygame.Rect(x,y,tile_size,tile_size))
            cut_tiles.append(new_surf)

    return cut_tiles