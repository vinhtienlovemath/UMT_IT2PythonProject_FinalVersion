level_0 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/0/level_0_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/0/level_0_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/0/level_0_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/0/level_0_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/0/level_0_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/0/level_0_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/0/level_0_tree.csv',
        'node_pos':(110,400),
        'unlock':1,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/0'}

level_1 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/1/level_1_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/1/level_1_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/1/level_1_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/1/level_1_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/1/level_1_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/1/level_1_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/1/level_1_tree.csv',
        'node_pos':(300,220),
        'unlock':2,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/1'}

level_2 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/2/level_2_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/2/level_2_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/2/level_2_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/2/level_2_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/2/level_2_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/2/level_2_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/2/level_2_tree.csv',
        'node_pos':(480,570),
        'unlock':3,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/2'}

level_3 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/3/level_3_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/3/level_3_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/3/level_3_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/3/level_3_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/3/level_3_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/3/level_3_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/3/level_3_tree.csv',
        'node_pos':(670,360),
        'unlock':4,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/3'}

level_4 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/4/level_4_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/4/level_4_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/4/level_4_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/4/level_4_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/4/level_4_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/4/level_4_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/4/level_4_tree.csv',
        'node_pos':(880,160),
        'unlock':5,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/4'}

level_5 = { 
        'terrain': '../GAME2D_IT2/2_Level/levels/5/level_5_terrain.csv',
        'coin': '../GAME2D_IT2/2_Level/levels/5/level_5_coin.csv',
        'constraint':'../GAME2D_IT2/2_Level/levels/5/level_5_constraint.csv',
        'enemies':'../GAME2D_IT2/2_Level/levels/5/level_5_enemies.csv',
        'grass':'../GAME2D_IT2/2_Level/levels/5/level_5_grass.csv',
        'player':'../GAME2D_IT2/2_Level/levels/5/level_5_player.csv',
        'tree':'../GAME2D_IT2/2_Level/levels/5/level_5_tree.csv',
        'node_pos':(1050,400),
        'unlock':5,
        'node_graphics': '../GAME2D_IT2/2_Level/graphics/overworld/5'}

levels = {
	0: level_0,
	1: level_1,
	2: level_2,
	3: level_3,
	4: level_4,
	5: level_5}



# Dẫn đến thư mục chứa tile