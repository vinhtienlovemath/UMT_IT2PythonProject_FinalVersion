import pygame
from support import import_folder
from math import sin
from music import Music

pygame.mixer.init()
effect_music = Music()

class Player(pygame.sprite.Sprite):
    # Tạo con nhân vật
    def __init__(self,pos,surface,create_jump_particles, change_health):
        super().__init__()
        self.import_character_assets()
        self.frame_index = 0
        self.animation_speed = 0.15     # Tạo tốc độ chuyển hoạt anh animation là 0.15s
        self.image = self.animations['idle'][self.frame_index]  #Set con player dến frame chứa 'idle' là thư mục chứa animation
        self.rect = self.image.get_rect(topleft = pos)
    
        # Tạo hiệu ứng bụi
        self.import_dust_run_particles()
        self.dust_frame_index = 0 
        self.dust_animation_speed = 0.15
        self.display_surface = surface
        self.create_jump_particles = create_jump_particles


        # Tạo cách thức di chuyển của con nv 
        self.direction = pygame.math.Vector2(0,0)
        self.speed = 8          #TỐc độ
        self.gravity = 0.8      #Trọng lực
        self.jump_speed = -16   #Tốc độ nhảy

        self.status = 'idle'
        self.facing_right = True
        self.on_ground = False
        self.on_ceiling = False
        self.on_left = False
        self.on_right = False

        self.change_health = change_health
        self.invincible = False
        self.invincibility_duration = 500
        self.hurt_time = 0

        # Audio
        self.jump_sound = effect_music.jump_sound
        self.jump_sound.set_volume(0.5)
        self.hit_sound = effect_music.hit_sound

       # Truy xuất đến file chứa các animation 
    def import_character_assets(self):
        character_path = '../GAME2D_IT2/2_Level/graphics/character/'
        self.animations = {'idle':[],'run':[],'jump':[], 'fall':[]}

        for animations in self.animations.keys():
            full_path = character_path + animations
            self.animations[animations] = import_folder(full_path)      # Add các animation từ tệp support.py tại đây đã có tạo vòng lặp xuất file

    
    # Truy xuất đến file chứa các animation bụi
    def import_dust_run_particles(self):
        self.dust_run_particles = import_folder('../GAME2D_IT2/2_Level/graphics/character/dust_particles/run/')

    # Tạo animation cho nv
    def animate(self):
        animation = self.animations[self.status]

        self.frame_index += self.animation_speed #          
        if self.frame_index >= len(animation):   # Nếu cái farme index lớn = độ dà của cái animation thì
            self.frame_index = 0
        
        image = animation[int(self.frame_index)]
        if self.facing_right:
            self.image = image
        else:
            filippe_image = pygame.transform.flip(image, True, False)           # Tạo hiệu ứng quay ngược lại của con nv
            self.image = filippe_image

        if self.invincible:
            alpha = self.wave_value()
            self.image.set_alpha(alpha)
        else:
            self.image.set_alpha(255)

        if self.on_ground and self.on_right:
            self.rect = self.image.get_rect(bottomright = self.rect.bottomright)

        elif self.on_ground and self.on_left:
            self.rect = self.image.get_rect(bottomleft = self.rect.bottomleft)
        elif self.on_ground:
            self.rect = self.image.get_rect(midbottom = self.rect.midbottom)      

        elif self.on_ceiling and self.on_right:
            self.rect = self.image.get_rect(topright = self.rect.topright)
        elif self.on_ceiling and self.on_left:
            self.rect = self.image.get_rect(topleft = self.rect.topleft)
        elif self.on_ground:
            self.rect = self.image.get_rect(midtop = self.rect.midtop)    
  
    # Tạo animation cho run
    def run_dust_animation(self):
        if self.status == 'run' and self.on_ground:
            self.dust_frame_index += self.dust_animation_speed
            if self.dust_frame_index >= len(self.dust_run_particles):
                self.dust_frame_index = 0
            dust_particle = self.dust_run_particles[int(self.dust_frame_index)]

            if self.facing_right:
                pos = self.rect.bottomleft - pygame.math.Vector2(6,15)
                self.display_surface.blit(dust_particle,pos)
            else:
                pos = self.rect.bottomright - pygame.math.Vector2(6,15)
                flipped_dust_particle = pygame.transform.flip(dust_particle,True,False)
                self.display_surface.blit(flipped_dust_particle,pos)
   
    # Tạo phím di chuyển cho nó
    def get_input(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_RIGHT]:
            self.direction.x = 1
            self.facing_right = True
        elif keys[pygame.K_LEFT]:
            self.direction.x = -1
            self.facing_right = False
        else:
            self.direction.x = 0
        if keys[pygame.K_SPACE] and self.on_ground:
            self.jump()
            self.create_jump_particles(self.rect.midbottom)
    
    # Add animation cho nv 
    def get_status(self):
        if self.direction.y < 0:
            self.status = 'jump'
        elif self.direction.y > 1:
            self.status = 'fall'
        else:
            if self.direction.x != 0:
                self.status = 'run'
            else:
                self.status = 'idle'

    # Tạo trọng lực       
    def apply_gravity(self):
        self.direction.y += self.gravity
        self.rect.y += self.direction.y

    # Tạo nhảy  
    def jump(self):
        self.direction.y = self.jump_speed
        self.jump_sound.play()

    def get_damage(self):
        if not self.invincible:
            self.hit_sound.play()
            self.change_health(-25)
            self.invincible = True
            self.hurt_time = pygame.time.get_ticks()
          
    def invincibility_timer(self):
        if self.invincible:
            current_time = pygame.time.get_ticks()
            if current_time - self.hurt_time >= self.invincibility_duration:
                self.invincible = False

    def wave_value(self):
        value = sin(pygame.time.get_ticks())
        if value >= 0: return 255
        else: return 0


    # Chạy source
    def update(self):
        self.get_input()
        self.get_status()
        self.animate()
        self.run_dust_animation()
        self.invincibility_timer()
        self.wave_value()

